<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Problem 1 : Numbers to Words</title>
</head>

<body>
<form action="index.php" method="post">
<table width="70%" border="0" align="center" cellpadding="5">
  <tr>
    <td colspan="2" align="center" bgcolor="#999999"><strong>Problem 1 : Numbers to words</strong></td>
    </tr>
  <tr>
    <td align="right" width="200"><strong>Enter number:</strong></td>
    <td>
      <input type="number" name="number" id="number" value="<?php echo $number;?>"></td>
    </tr>
  <tr>
    <td align="right" width="200">&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Submit"></td>
  </tr>
  <tr>
    <td align="right" width="200"><strong>You Entered:</strong></td>
    <td bgcolor="#CCCCCC" style="font-style:italic">
	<?php 
	
	include("numbersToWords.php"); 
	
if(!isset($_POST['number'])){
	
	echo "No Number entered.";
		
} else {
		$number= $_POST['number'];
	if($number>2500){
		echo "Sorry, You entered a number greater than 2500!";
		}
		else if($number<0){
		echo "Sorry, You entered a number less than 0!";
		}
		else{
			echo $number. "  =  ";
		echo number_to_words($number);
		}
}
?></td>
    </tr>
    <tr>
    <td colspan="2" bgcolor="#999999" align="center">Timothy Namanya</td>
    </tr>
</table>

</form>
</body>
</html>
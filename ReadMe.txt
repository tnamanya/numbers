README:
During my coding, I used both wamp server and xampp, so I recommend an APACHE server
to run PHP and Mysql databases any other servers can be used but these are what I used.

problem 1 Installation

I Used PHP and HTML for this problem and all you need is to extract the numbers.zip
archive into your root directory and run it through your browser.
you should get an output consistent with the snapshots of the application.
NOTE THAT: the application only returns numbers in words from 0 to 2500 ONLY.


Problem 2
The application is for top management to view the progress of the stores and
process the input excel files to produce a report output.
I Used procedural programming with function calls and definitions plus a few
ocassions of Object Oriented Programming. 

Due to time, I couldn't complete some sections but I will submit my work anyway!!

Complete Areas include:
---------------------------
-Dashboard view of the monthly report
-Upload of files
-Most Selling Items
-Profit made on every product
-Security is key, so I had to work on that a well
-Ease in manipulation since all data is uploaded into a database

incomplete Areas include:
----------------------------
-Viewing Performance by Product type
-Viewing Performance by month
-Upload of files only allows CSV files with comma delimiters 
all xlsx files have to be saved as .csv with comma delimiters before upload. 


Installation:
Extract the kcs.zip into the root directory and create 'kcs' database 
Now import kcs.sql from the kcs folder to load the tables.
Database has no password.

Finally run the project in your browser usually but not limited to the location:

localhost/kcs/index.php
Username: admin
Password: admin
Database has no password.
